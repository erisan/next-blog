

const data = [
    {
      title: "How To Setup SEO Friendly Codebase When Using nextjs",
      content: `Lorem ipsum dolor sit amet. Quo placeat distinctio est illo iusto sit commodi rerum sit doloremque fugiat et enim asperiores ut voluptatem quam! Non nobis minus At tempora enim et voluptatum vitae 33 perspiciatis quia non minus voluptatibus ut voluptates totam qui galisum omnis.
  
      Sed voluptates sunt sit deserunt velit non similique delectus? Et laborum voluptatem est minima similique eum itaque consectetur est nesciunt aspernatur sed enim accusantium? Aut inventore Quis aut ipsum quisquam et deserunt velit est ratione placeat aut accusamus asperiores et facilis dolores. Non accusamus consectetur est corrupti dolorum hic dolorum labore?
      
      Ea ratione assumenda ea animi accusamus ut deleniti natus ut dolorem tenetur id corporis ut quibusdam ipsum eum nihil enim! Ut perspiciatis galisum et porro totam rem consequatur architecto sed illum iusto ut quas expedita in cumque repellat`,
      cover: "https://images.pexels.com/photos/12258844/pexels-photo-12258844.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
      slug: "/how-to-set-up-cidebase-in-next",
    },
    {
      title: "How To Become A Nextjs Developer In 2022",
      content: `Lorem ipsum dolor sit amet. Quo placeat distinctio est illo iusto sit commodi rerum sit doloremque fugiat et enim asperiores ut voluptatem quam! Non nobis minus At tempora enim et voluptatum vitae 33 perspiciatis quia non minus voluptatibus ut voluptates totam qui galisum omnis.
  
      Sed voluptates sunt sit deserunt velit non similique delectus? Et laborum voluptatem est minima similique eum itaque consectetur est nesciunt aspernatur sed enim accusantium? Aut inventore Quis aut ipsum quisquam et deserunt velit est ratione placeat aut accusamus asperiores et facilis dolores. Non accusamus consectetur est corrupti dolorum hic dolorum labore?
      
      Ea ratione assumenda ea animi accusamus ut deleniti natus ut dolorem tenetur id corporis ut quibusdam ipsum eum nihil enim! Ut perspiciatis galisum et porro totam rem consequatur architecto sed illum iusto ut quas expedita in cumque repellat`,
      cover: "https://images.pexels.com/photos/12258844/pexels-photo-12258844.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
      slug: "/how-to-become-a-next-js-dev"
    },
    {
      title: "How To Make Passive Income As A Frontend Developer",
      content: `Lorem ipsum dolor sit amet. Quo placeat distinctio est illo iusto sit commodi rerum sit doloremque fugiat et enim asperiores ut voluptatem quam! Non nobis minus At tempora enim et voluptatum vitae 33 perspiciatis quia non minus voluptatibus ut voluptates totam qui galisum omnis.
  
      Sed voluptates sunt sit deserunt velit non similique delectus? Et laborum voluptatem est minima similique eum itaque consectetur est nesciunt aspernatur sed enim accusantium? Aut inventore Quis aut ipsum quisquam et deserunt velit est ratione placeat aut accusamus asperiores et facilis dolores. Non accusamus consectetur est corrupti dolorum hic dolorum labore?
      
      Ea ratione assumenda ea animi accusamus ut deleniti natus ut dolorem tenetur id corporis ut quibusdam ipsum eum nihil enim! Ut perspiciatis galisum et porro totam rem consequatur architecto sed illum iusto ut quas expedita in cumque repellat`,
      cover: "https://images.pexels.com/photos/12258844/pexels-photo-12258844.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
      slug: "/how-to-make-money-as-a-dev"
    },
    {
      title: "Hello World! What does it mean?",
      content: `Lorem ipsum dolor sit amet. Quo placeat distinctio est illo iusto sit commodi rerum sit doloremque fugiat et enim asperiores ut voluptatem quam! Non nobis minus At tempora enim et voluptatum vitae 33 perspiciatis quia non minus voluptatibus ut voluptates totam qui galisum omnis.
      Sed voluptates sunt sit deserunt velit non similique delectus? Et laborum voluptatem est minima similique eum itaque consectetur est nesciunt aspernatur sed enim accusantium? Aut inventore Quis aut ipsum quisquam et deserunt velit est ratione placeat aut accusamus asperiores et facilis dolores. Non accusamus consectetur est corrupti dolorum hic dolorum labore?
      Ea ratione assumenda ea animi accusamus ut deleniti natus ut dolorem tenetur id corporis ut quibusdam ipsum eum nihil enim! Ut perspiciatis galisum et porro totam rem consequatur architecto sed illum iusto ut quas expedita in cumque repellat`,
      cover: "https://images.pexels.com/photos/12258844/pexels-photo-12258844.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
      slug: "/hello-world"
    },
    {
      title: "How To Setup SEO Friendly Codebase When Using nextjs",
      content: `Lorem ipsum dolor sit amet. Quo placeat distinctio est illo iusto sit commodi rerum sit doloremque fugiat et enim asperiores ut voluptatem quam! Non nobis minus At tempora enim et voluptatum vitae 33 perspiciatis quia non minus voluptatibus ut voluptates totam qui galisum omnis.
  
      Sed voluptates sunt sit deserunt velit non similique delectus? Et laborum voluptatem est minima similique eum itaque consectetur est nesciunt aspernatur sed enim accusantium? Aut inventore Quis aut ipsum quisquam et deserunt velit est ratione placeat aut accusamus asperiores et facilis dolores. Non accusamus consectetur est corrupti dolorum hic dolorum labore?
      
      Ea ratione assumenda ea animi accusamus ut deleniti natus ut dolorem tenetur id corporis ut quibusdam ipsum eum nihil enim! Ut perspiciatis galisum et porro totam rem consequatur architecto sed illum iusto ut quas expedita in cumque repellat`,
      cover: "https://images.pexels.com/photos/12258844/pexels-photo-12258844.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
      slug: "/how-to-set-up-cidebase-in-next",
      
    },
    {
      title: "What is React? Is it Worth Learning?",
      content: `Lorem ipsum dolor sit amet. Quo placeat distinctio est illo iusto sit commodi rerum sit doloremque fugiat et enim asperiores ut voluptatem quam! Non nobis minus At tempora enim et voluptatum vitae 33 perspiciatis quia non minus voluptatibus ut voluptates totam qui galisum omnis.
  
      Sed voluptates sunt sit deserunt velit non similique delectus? Et laborum voluptatem est minima similique eum itaque consectetur est nesciunt aspernatur sed enim accusantium? Aut inventore Quis aut ipsum quisquam et deserunt velit est ratione placeat aut accusamus asperiores et facilis dolores. Non accusamus consectetur est corrupti dolorum hic dolorum labore?
      
      Ea ratione assumenda ea animi accusamus ut deleniti natus ut dolorem tenetur id corporis ut quibusdam ipsum eum nihil enim! Ut perspiciatis galisum et porro totam rem consequatur architecto sed illum iusto ut quas expedita in cumque repellat`,
      cover: "https://images.pexels.com/photos/12258844/pexels-photo-12258844.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
      slug: "/react"
    }
  ]


  module.exports = data
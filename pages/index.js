import React from 'react'

export default function index() {
  return (
    <div>
      <h2>Endpoints</h2>
      <div>base url: https://next-blogs-ten.vercel.app/api/</div>

      <ul>
      <li>
          Get all blog (get) - "/blog"
        </li>
        <li>
          Get all single blog (get) - "/blog/uid"
        </li>
      </ul>
    </div>
   
  )
}

import React from 'react'
import Layout from '../layout'

export default function BlogDetails({ posts }) {

  return (

    <Layout title={posts?.title} description={posts?.content.split(".")[0]}>

      <div>
        <img src={posts?.cover} />

        <h1>{posts?.title}</h1>
        <div>
          {posts?.content}
        </div>
      </div>
    </Layout>

  )
}


// This function gets called at build time
export async function getStaticProps({ params }) {
  console.log(params, "params")
  const res = await fetch(`https://next-blogs-ten.vercel.app/api/blog/${params.uid}`)
  const posts = await res.json()

  return {
    props: {
      posts,
    },
  }
}

export async function getStaticPaths() {
  const res = await fetch("https://next-blogs-ten.vercel.app/api/blog")
  const posts = await res.json()

  const paths = posts?.map((post) => {
    return ({
      params: { uid: post.slug },
    })
  })

  return { paths: paths, fallback: true }
}
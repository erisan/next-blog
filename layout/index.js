import React from "react";
import Head from "next/head";


const Layout = ({
    title,
    description,
    children,
}) => {

    return (
        <>
            <Head>
                <meta charSet="utf-8" />
                <meta httpEquiv="x-ua-compatible" content="ie=edge" />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <title>{title}</title>
                <meta name="description" content={description} />
                <meta name="robots" content="index,follow" />
                <meta name="googlebot" content="index,follow" />
                <meta name="description" content={description} />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:creator" content="@creator_twitter_handle" />
                <meta name="twitter:site" content="@your_plaform_twitter_handle" />
                <meta property="og:url" content="your_domain_URL" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={``} />
                <meta property="og:image" content="../static/img/bitpowrView2.png" />
                <meta property="og:description" content={description} />
                <meta property="og:image:alt" content="Blockchain Payment Platform" />
                <meta property="og:image:width" content="1253" />
                <meta property="og:image:height" content="740" />
                <meta property="og:locale" content="en_US" />
                <meta property="og:site_name" content={title} />
                <meta
                    name="twitter:image"
                    content="https://bitpowr.com/static/img/bitpowrView2.png"
                />
                <link rel="canonical" href="https://bitpowr.com" />
            </Head>

            <div>
                {children}
            </div>

        </>
    );
}

export default Layout;

import Layout from '../../layout'
import Link from "next/link"

export default function Home(props) {

  if (props?.error) {
    return <h1>Error occurred, try again</h1>
  }

  return (
    <Layout>
      <h1>Recent Post</h1>

      <ul>
        {
          props.data?.map((data, index) => {
            return (
              <li key={index}>
                <Link href={data?.slug}>
                  <a>
                    {data.title}
                  </a>
                </Link>

              </li>
            )
          })
        }
      </ul>
    </Layout>
  )
}

export async function getStaticProps(context) {

  var error = false
  var data = []

  await fetch("https://next-blogs-ten.vercel.app/api/blog")
    .then((res => res.json()))
    .then(res => data = res)
    .catch(error => error = true)


  return {
    props: { data, error },
  }
}